class User < ActiveRecord::Base
  
  has_paper_trail

  validates :pesel, uniqueness: true, pesel_format: true

end
