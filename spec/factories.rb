FactoryGirl.define do
  factory :user do
    sequence(:name) { |n| "name#{n}" }
    sequence(:surname) { |n| "surname#{n}" }
    pesel "87030700096"
  end
end