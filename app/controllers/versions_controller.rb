class VersionsController < ApplicationController

  before_action :set_version

  def revert
    if @version.reify
      @version.reify.save!
    else
      @version.item.destroy
    end
    redirect_to :back, :notice => "Undid #{@version.event}. #{revert_link}"
  end

  private
    def set_version
      @version = Version.find(params[:id])
    end

    def revert_link
      link_name = params[:redo] == "true" ? "Undo" : "Redo"
      view_context.link_to(link_name, revert_version_path(@version.next, redo: !params[:redo]), method: :post)
    end
end