class PeselFormatValidator < ActiveModel::EachValidator
  def validate_each(object, attribute, value)
    
    eb = unless value =~ /^[0-9]{11}$/
      false
    else
      wagi = [1, 3, 7, 9, 1, 3, 7, 9, 1, 3]
      intSum = 0
      (0..9).each do |i|
        intSum += wagi[i] * value[i].to_i #mnożymy każdy ze znaków przez wagć i sumujemy wszystko
      end
      int = 10 - intSum % 10 #suma kontrolna
      intControlNr = int == 10 ? 0 : int
      intControlNr == value[10].to_i ? true : false #sprawdzamy czy taka sama suma kontrolna jest w ciągu
    end

    object.errors.add(attribute, :pesel_format, options) unless eb

  end
end