require 'spec_helper'

describe User do
  describe "#create new user" do
    let(:user) { FactoryGirl.create(:user) }

    it "should create user with valid data" do
      user.should be_valid
    end

    it "should not be valid if pesel is incorrect" do
      user_invalid = FactoryGirl.build(:user, pesel: "87030700097") #niepoprawny numer pesel
      user_invalid.should_not be_valid
      user_invalid.should have(1).error_on(:pesel)
    end

    it "should not create user with existing pesel" do
      user.should be_valid
      user2 = FactoryGirl.build(:user)
      user2.should_not be_valid
      user2.should have(1).error_on(:pesel) 
    end

  end
end