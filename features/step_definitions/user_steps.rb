def create_user
  @user = FactoryGirl.create(:user)
end

Given /^I have no users$/ do
  User.delete_all
end

When(/^I create user with valid data$/) do
  create_user
end

When(/^I create user with invalid pesel$/) do
  FactoryGirl.build(:user, pesel: "87030700097")
end

When(/^I build user with valid data$/) do
  u = FactoryGirl.build(:user)
  u.save
end

When /^I follow "([^\"]*)"$/ do |link|
  click_link(link)
end

Then /^I should have ([0-9]+) users?$/ do |count|
  User.count.should == count.to_i
end

Then /^I have user pesel (.+)$/ do |name|
  @user.pesel.should == name
end

