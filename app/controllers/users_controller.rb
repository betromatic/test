class UsersController < ApplicationController
  before_action :set_user, only: [:show, :edit, :update, :destroy]

  def index
    @users = User.all
  end

  def show
  end

  def new
    @user = User.new
  end

  def edit
  end

  def create
    @user = User.new(user_params)

    if @user.save
      redirect_to users_url, notice: "User was successfully created. #{undo_link}"
    else
      render action: 'new'
    end
  end

  def update
    if @user.update(user_params)
      redirect_to users_url, notice: "Successfully updated product. #{undo_link}"
    else
      render action: 'edit'
    end
  end

  def destroy
    @user.destroy
    redirect_to users_url, notice: "User was successfully destroyed. #{undo_link}"
  end

  private
    def set_user
      @user = User.find(params[:id])
    end

    def user_params
      params.require(:user).permit(:name, :surname, :pesel)
    end

    def undo_link
      view_context.link_to("Undo", revert_version_path(@user.versions.scoped.last), method: :post)
    end
end
