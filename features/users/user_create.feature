Feature: Create
  In order to get access to create user sections of the site
  As an guest
  I want to create and manage users
     
  Scenario: Guest create new user with valid data
    Given I have no users
    When I create user with valid data
    Then I should have 1 user

  Scenario: Guest create new user with invalid data
    Given I have no users
    When I create user with invalid pesel
    Then I should have 0 user

  Scenario: Guest create new user with existing pesel
    Given I have no users
    When I create user with valid data
    Then I should have 1 user
    Then I build user with valid data
    Then I should have 1 user